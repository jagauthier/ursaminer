#!/usr/bin/python

import sys
import getopt
import socket
import time
import re
import random
import datetime
import json
from time import strftime

### VARIABLES TO CHANGE DEFAULT CONNECTION
defaulthost = "home.pendulus.org" # the ip address of the machine running the miner
defaultport = 5000   # the port number to connect to
defaultrefresh = 5         # the time (in seconds) to wait until refreshing

###

def connect_to_miner(host, port):
    s = None
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except:
        print "Socket error."
        s = None
    try:
        port = int(port)
        s.connect((host, port))
    except:
        print "Connection error."
        raise
        s = None
    return s

def request(socket, request_type):
    if socket is None:
        return None
    try:
        socket.sendall(request_type)
    except:
        print "Request error."
        return None
    r = socket.recv(4096)
    return r

def usage():
    print(sys.argv[0] + " -h -o <host> -p <port> -r <refresh>")

def convert_khs(khs):
    hash = float(khs)
    if hash>10000:
        return "{:.2f}".format(hash/1000)+ " MH/s"
    else:
        return str(hash)+ " kH/s"

def main(argv):
    host =  defaulthost
    port = defaultport
    refresh = defaultrefresh

    try:
        opts, arts = getopt.getopt(argv,"a:h:o:p:r:i:u:d:", ['host=','port=','refresh='])
    except getopt.GetoptError:
        usage()
        sys.exit(0)

    accepted_t = 0
    rejected_t = 0
    gpus_copy = {}
    iter=0
    while True:
        now = datetime.datetime.now()

        now_str = now.strftime("[%Y-%d-%m %H:%M:%S]")
        s = connect_to_miner(host, port)
        r = request(s, "pool")
        if r is not None:
            d = dict(x.split('=') for x in r.split(';'))
            block=d['H']

        s = connect_to_miner(host, port)
        r = request(s, "summary")

        if r is not None:
            d = dict(x.split('=') for x in r.split(';'))
            algo=d['ALGO']
            accepted=d['ACC']
            rejected=d['REJ']
            khs=d['KHS']
            diff=float(d['DIFF'])/100000

        s = connect_to_miner(host, port)
        r = request(s, "threads")


        if r is not None:
            r.strip()
            gpus={}
            gpu_info=r.split('|')
            for gpu in gpu_info:
                if len(gpu)>1:
                    d = dict(x.split('=') for x in gpu.split(';'))
                    this=d['GPU']
                    gpus[this]={'GPU': d['GPU'], 'CARD': d['CARD'], 'KHS': d['KHS']}


        total = int(accepted) + int (rejected)
        if iter>1:
            if accepted_t != accepted:
                print now_str + " accepted: " + accepted_t + "/" + str(total),
                print " (diff " + str(diff) + "), " + convert_khs(khs)+ " yes!"
            if rejected_t != rejected:
                print now_str + " accepted: " + accepted_t + "/" + str(total),
                print " (diff " + diff + "), hashrate booooo"
        iter+=1
        if gpus_copy:
            for gpu in gpus:
                if gpus[gpu]['KHS'] != gpus_copy[gpu]['KHS']:
                    print now_str + " GPU #" + gpus[gpu]['GPU'] + ": " +  gpus[gpu]['CARD'] + ", ",
                    print convert_khs(gpus[gpu]['KHS'])


        gpus_copy = gpus.copy()
        accepted_t = accepted
        rejected_t = rejected

        sys.stdout.flush()
        time.sleep(.5)

if __name__ == "__main__":
    main(sys.argv[1:])
