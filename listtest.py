#!/usr/bin/python
from collections import defaultdict
import random
import ast
import pprint
from itertools import groupby
from operator import itemgetter
profits=[{'algo': 'neoscrypt',
  'gpu': 'total',
  'pool': 'nicehash',
  'profit': 6.583635340833725},
 {'algo': 'neoscrypt',
  'gpu': 'total',
  'pool': 'zpool',
  'profit': 6.477506528436841},
 {'algo': 'phi', 'gpu': 'total', 'pool': 'zpool', 'profit': 5.556168123475228},
 {'algo': 'x17', 'gpu': 'total', 'pool': 'zpool', 'profit': 4.977350081042117},
 {'algo': 'skein',
  'gpu': 'total',
  'pool': 'zpool',
  'profit': 4.861145534703928},
 {'algo': 'neoscrypt',
  'gpu': '0',
  'pool': 'nicehash',
  'profit': 3.5584361786392287},
 {'algo': 'neoscrypt',
  'gpu': '0',
  'pool': 'zpool',
  'profit': 3.5010738573565217},
 {'algo': 'phi', 'gpu': '0', 'pool': 'zpool', 'profit': 3.3434526247447405},
 {'algo': 'x17', 'gpu': '0', 'pool': 'zpool', 'profit': 3.0118176177374516},
 {'algo': 'skein', 'gpu': '0', 'pool': 'zpool', 'profit': 2.917606841542478},
 {'algo': 'neoscrypt',
  'gpu': '1',
  'pool': 'nicehash',
  'profit': 2.5775598460143354},
 {'algo': 'neoscrypt',
  'gpu': '1',
  'pool': 'zpool',
  'profit': 2.5360093422003196},
 {'algo': 'phi', 'gpu': '1', 'pool': 'zpool', 'profit': 2.2127154987514386},
 {'algo': 'x17', 'gpu': '1', 'pool': 'zpool', 'profit': 1.9655324632861255},
 {'algo': 'skein', 'gpu': '1', 'pool': 'zpool', 'profit': 1.94353869316145},
 {'algo': 'tribus',
  'gpu': 'total',
  'pool': 'zpool',
  'profit': 1.574455379589875},
 {'algo': 'tribus', 'gpu': '0', 'pool': 'zpool', 'profit': 0.9334546030644119},
 {'algo': 'sib',
  'gpu': 'total',
  'pool': 'zpool',
  'profit': 0.8048240260864019},
 {'algo': 'tribus', 'gpu': '1', 'pool': 'zpool', 'profit': 0.6410007765254631},
 {'algo': 'sib', 'gpu': '0', 'pool': 'zpool', 'profit': 0.48682192822929793},
 {'algo': 'sib', 'gpu': '1', 'pool': 'zpool', 'profit': 0.31800209785835765}]

algos="x17,skein,neoscrypt,phi,tribus,sib"

devices="total,0,1,2,3,4,5,6,7"

# profits=[]
# for algo in algos.split(","):
#     for device in devices.split(","):
#         profits.append({'algo': algo, 'gpu': device, 'pool': 'zpool',
#                         'profit': random.uniform(1, 3)})
# for algo in algos.split(","):
#     for device in devices.split(","):
#         profits.append({'algo': algo, 'gpu': device, 'pool': 'nicehash',
#                         'profit': random.uniform(1, 3)})

profits.sort(key=itemgetter('profit'), reverse=True)

profits = [p  for p in profits if p['gpu'] != 'total']

pprint.pprint(profits)
print
print

def uniq(l):
    uniq=[]
    for device in devices.split(","):
        for p in profits:
#            print p['gpu']
            if p['gpu'] == device:
                uniq.append(p)
                break
    return uniq
profits=uniq(profits)
print
pprint.pprint(profits)
print
profits.sort(key=itemgetter('algo'), reverse=True)
pprint.pprint(profits)
print
mine=[]
for algo, items in groupby(profits, key=itemgetter('algo','pool')):

    md=[]
    profit=0
    for i in items:
        md.append(i['gpu'])
        profit+=i['profit']
    devices = ",".join(md)
    t={'algo': algo[0], 'pool': algo[1], 'devices': devices,
           'profit':profit}
    print t
    mine.append({'algo': algo[0], 'pool': algo[1], 'devices': devices,
           'profit':profit})
#pprint.pprint(mine)
