#!/usr/bin/python
import time
import requests
import Queue
import logging
import ConfigParser
import traceback
import os
from threading import Thread
from utils import utils


# This is class that contains common pool functions
log = logging.getLogger(__name__)

class pool():

    my_info = {}
    # used to get the data, internally
    pool_info = {}
    # local reference to config
    config = None
    # requests object
    client = None
    # queue used so we don't have a thread variable conflict
    queue = Queue.Queue()

    my_algos = None

    # pool monitoring vars
    first_pass = True
    # a list of algo status per pool (tcp checks)
    algo_status = {}
    # This will start false, because until we get initial
    # API data, we're considering the pool down, ie: not minable.

    pool_name = ""

    down_counter = 10

    def __init__(self, pool_handler):
        self.pool_name = self.__class__.__name__
        self.pool_handler = pool_handler
        self.config = pool_handler.config
        self.client = requests.session()
        log.debug("Starting thread: %s", self.pool_name)
        t = Thread(target=self.run,
                   name='{}'.format(self.pool_name), args=())
        t.daemon = True
        t.start()

    # after the algos are validated on the pool, we set them back here
    # so we can used them for things.
    def set_algos(self, algos):
        self.my_algos = algos
        log.debug("Algos set: '%s'", self.my_algos)

    # this is the thread safe version.
    def get_pool_data(self):
        log.verbose("Getting pool data from queue.")
        pool_data=self.queue.get()
        self.queue.put(pool_data)
        return pool_data

    def run(self):
        while True:
            log.debug("Getting %s data.",self.pool_name)
            self.get_pool_info()

            log.verbose("Sleeping.")
            time.sleep(60)

    # Different pools call things differently.
    # We want to respect their hostname for mining,
    # But we want to use one internal algo name
    def map_algo(self, algo):
        if self.pool_name == 'nicehash':
            if algo == 'lyra2rev2':
                return 'lyra2v2'
        
        return algo
        
    # Gets the pool data.
    # algos, workers, estimates, etc
    # we won't use it all.
    def get_pool_info(self):
        error = True
        down = 0
        while (error is True):
            try:
                response = self.client.get(self.pool_status)
                data = response.json()
                self.process_pool_data(data)
                # used in pool status checks
                # clear the queue, we only need one item in it
                with self.queue.mutex:
                    self.queue.queue.clear()
#                log.debug("Put new data in queue.")
                self.queue.put(self.pool_info)
                if self.first_pass:
                    self.validate_algorithms()
                self.first_pass = False
                self.pool_handler.pool_status[self.pool_name]['alive'] = True
                error = False
                if down > self.down_counter:
                    log.info("%s is up.", self.pool_name)
                down = 0
            except:
                down = down + 1
                # simple... ten tries and we're down.
                if down == self.down_counter:
                    self.pool_handler.pool_status[self.pool_name]['alive'] = False
                    log.warn("%s is down.", self.pool_name)
                if down > self.down_counter - 1:
                    # Clear the queue, we don't want to consider this pool
                    with self.queue.mutex:
                         self.queue.queue.clear()
                    time.sleep(down)
                else:
                    exceptiondata = traceback.format_exc().splitlines()
                    log.error("%s API error. Attempt: %i/%i. '%s' ",
                               self.pool_name, down, self.down_counter,
                               exceptiondata[-1])
                    time.sleep(11)



    def check_algo_status(self, algo, host, port):
        # This will reduce TCP checking on algos we aren't interested in but
        # still reported by the pool.

        if self.my_algos is not None:
            if algo not in self.my_algos:
                return False

        status = utils.check_socket(host, port)

        output = False
        # if it's in the dict, it's already been checked at some point
        if algo in self.algo_status:
            if self.algo_status[algo] != status:
                # changed status
                output = True
        # never been checked.
        else:
            output = True

        self.algo_status[algo] = status

        # I just don't like seeing a status of the ports
        # the first time the pool loads.
        if self.first_pass:
            return status

        if output and status:
            log.info("Algo '%s' on '%s' is up.",
              algo, self.pool_name)
        if output and not status:
            log.info("Algo '%s' on '%s' is down.",
              algo, self.pool_name)

        return status

    # This will validate the algorithms you've selected are legitimate
    # and the alogorithms are supported on the pool.
    # Use a lock - we only want this to run once per pool at a time.
    def validate_algorithms(self):
        # before we can validate the algorithms, the pools must have queried
        # That's what the queue and pool_data is for.
        with self.pool_handler.lock:
            log.info("Validating algorithms on %s.", self.pool_name)
            valid_pool_algo=[]
            # loop through configured algos, validate them against the pool
            for algo in self.config[self.pool_name]['algos'].split(","):
                if algo not in self.get_pool_data():
                    log.warning("Algo '%s' is not supported by pool '%s'",
                                algo, self.pool_name)
                else:
                    # This was created in utils.py, load_config
                    valid_pool_algo.append(algo)
                    self.config['algos'][algo]['needs_benchmark'] = True
            self.config[self.pool_name]['algos'] = ",".join(valid_pool_algo)
            self.set_algos(self.config[self.pool_name]['algos'])
            # Assign benchmarks.. Stay within the lock
            self.assign_bench()
            self.benchmark()

    # loop through the algos and if there is a speed assigned then
    # we'll create a dict of speeds on the algo, and set a flag for
    # benchmarking that we can use later.
    # Use a lock - we only want this to run once per pool at a time.
    def assign_bench(self):
        config_data = ConfigParser.ConfigParser()
        config_data.read(self.config['benchmark'])
        for algo in self.config[self.pool_name]['algos'].split(","):
            # If we're here it means we've specified an algo that is valid
            # on a pool.  Set the initial value to True for benchmarking
            self.config['algos'][algo]['needs_benchmark'] = True

            # set all speeds to 0, so profits will be 0 until benchmarked
            self.config['algos'][algo]['speed'] = {}
            self.config['algos'][algo]['speed']['total'] = 0
            for device in self.config['devices'].split(","):
                self.config['algos'][algo]['speed']["gpu"+device] = 0

            # If the section exists, then we know that we've stored the
            # benchmark data
            if config_data.has_section(algo):
                self.config['algos'][algo]['needs_benchmark'] = False
                self.config['algos'][algo]['speed'] = {}
                # This will read the speed for each GPU and a total
                total = config_data.getfloat(algo, 'total')
                self.config['algos'][algo]['speed']['total'] = total
                # if you change devices and don't benchmark.. this is gonna
                # break
                for device in self.config['devices'].split(","):
                    if not config_data.has_option(algo, "gpu"+device):
                        log.error("Yikes. It looks like your devices list doesn't match " + 
                                  "the benchmark data. If you added a device you need to " +
                                  "re-benchmark by deleting the " + self.config['benchmark'] +
                                  " file.")
                        os._exit(0)
                    gpu_speed = config_data.getfloat(algo, "gpu"+device)
                    self.config['algos'][algo]['speed']["gpu"+device] = gpu_speed

    def benchmark(self):
        print self.pool_name
        for algo in self.my_algos.split(","):
            if len(algo) < 1:
                continue
            if self.config['algos'][algo]['needs_benchmark']:
                # Don't queue up multiples
                if self.pool_handler.mining.is_benchmarking(algo):
                    continue
                port = self.config['algos'][algo]['miner']['port']
                api = self.config['algos'][algo]['miner']['api']
                log.info("Queueing benchmark '%s' on '%s'", algo, self.pool_name)
                mine_data = (self.pool_name, algo, port, api, self.config['devices'], True)
                self.pool_handler.mining.submit_mining(mine_data)


    def benchmark_orig(self):
        queued = {}
        for pool in self.config['pools']:
            for algo in self.config[pool]['algos'].split(","):
                if algo in queued or len(algo) < 1:
                    continue
                if self.config['algos'][algo]['needs_benchmark']:
                    queued[algo] = True
                    port = self.config['algos'][algo]['miner']['port']
                    api = self.config['algos'][algo]['miner']['api']
                    log.info("Queueing benchmark '%s' on '%s'", algo, pool)
                    # set last item to true for benchmarking
                    mine_data = (pool, algo, port, api, self.config['devices'], True)
                    self.submit_mining(mine_data)
