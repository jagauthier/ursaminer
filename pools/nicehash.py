#!/usr/bin/python
import logging
from pools import pool

log = logging.getLogger(__name__)

class nicehash(pool):
    # These are unique per pool
    my_url = "https://api.nicehash.com/api?method=stats.provider&addr="
    pool_status = "https://api.nicehash.com/api?method=simplemultialgo.info"
    divisor = 1000000000

    def __init(self, pool_handler):
        pool.__init__(self, pool_handler)

    def process_pool_data(self, data):
        location = self.config[self.__class__.__name__]['location']

        for algo_line in data['result']['simplemultialgo']:
            divisor = self.divisor
            name = algo_line['name']
            algo = self.map_algo(name)

            if float(algo_line['paying']) > 0:
                stratum_host=name + "." + location + '.nicehash.com'
                port = algo_line['port']

                status = self.check_algo_status(name, stratum_host, port)
                if status == False:
                    continue
                # nicehash is 4% for an external wallet, and
                # 2% for an internal.
                fees = 4
                if 'wallet' in self.config[self.pool_name]:
                    if self.config[self.pool_name]['wallet'] == 'internal':
                        fees = 2

                self.pool_info[algo] = {'algo': algo,
                             'port': algo_line['port'],
                             'fees': fees,
                             'paying': algo_line['paying'],
                             'divisor': divisor,
                             'btcghday': float(algo_line['paying'])/
                                         divisor*1000000000,
                             'stratum_host': 'stratum+tcp://' + stratum_host + ":" +
                                             str(port)}


    def lookup_algo(self, algo):
        if algo == 0:
            return "scrypt"
        if algo == 5:
            return "keccak"
        if algo == 7:
            return "nist5"
        if algo == 8:
            return "neoscrypt"
        if algo == 8:
            return "neoscrypt"
        if algo == 9:
            return "lyra2re"
        if algo == 22:
            return "cryptonight"
        if algo == 23:
            return "lbry"
        if algo == 24:
            return "equihash"
        if algo == 25:
            return "pascal"
        if algo == 26:
            return "x11gost"
        if algo == 28:
            return "blake2s"
        if algo == 29:
            return "skunk"

        return "unknown: " + str(algo)
