#!/usr/bin/python

import logging
from pools import pool
log = logging.getLogger(__name__)

class zpool(pool):

    # Pool specific items
    divisor = 1000000
    my_url = "http://www.zpool.ca/api/walletEx?address="
    pool_status = "http://www.zpool.ca/api/status"

    def __init(self, pool_handler):
        pool.__init__(self, pool_handler)

    def process_pool_data(self, data):

        for algo in data:
            d = data[algo]
            divisor = self.divisor
            if algo == "equihash":
                divisor /= 1000
            if algo == "blake2s":
                divisor *= 1000
            if algo == "blakecoin":
                divisor *= 1000
            if algo == "decred":
                divisor *= 1000
            if algo == "keccak":
                divisor *= 1000
            stratum_host=algo + '.mine.zpool.ca'
            port = d['port']
            status = self.check_algo_status(algo, stratum_host, port)
            if status == False:
                continue
            self.pool_info[algo] = {'algo': algo,
                         'port': d['port'],
                         'coins': d['coins'],
                         'fees': d['fees'],
                         'hashrate': d['hashrate'],
                         'workers': d['workers'],
                         'divisor': divisor,
                         'btcghday': (float(d['estimate_current'])/
                                     divisor)*1000000000,
                         'estimate_current': d['estimate_current'],
                         'estimate_last24h': d['estimate_last24h'],
                         'actual_last24h': d['actual_last24h'],
                         'hashrate_last24h': d['hashrate_last24h'],
                         'stratum_host': 'stratum+tcp://' +
                                         stratum_host + ":" +
                                         str(d['port'])}
