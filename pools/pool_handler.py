#!/usr/bin/python
import logging
import traceback
import requests
import ConfigParser
import time
import datetime
import Queue
import pprint
from utils import utils
from threading import Thread, Lock
from itertools import groupby
from operator import itemgetter

from zpool import zpool
from nicehash import nicehash
from collections import OrderedDict

# This is going to be the pool handler for all the pools.
CBBTC = "https://api.coinbase.com/v2/exchange-rates?currency=BTC"

log = logging.getLogger(__name__)

class pool_handler():
    config = None
    active_pools = {}

    # Pointer back to mining thread
    mining = None

    # I want these to load in order as they are in the CSV, and then
    # be retrieved in said order. This makes it easy to assign a "primary"
    # pool, and secondaries.
    pool_status = OrderedDict()

    # When this gets initialized, we're going to call it the startup time.
    # We use it in the main_loop. Read below for whys...
    startup_time = datetime.datetime.now()

    # threading can be a pain when it comes to variables.
    profits_queue = Queue.Queue()
    BTC_queue = Queue.Queue()
    lock = Lock()

    def __init__(self, config):
        self.config = config
        t = Thread(target=self.poll_BTC,
                   name='poll-BTC'.format(self.__class__.__name__), args=())
        t.daemon = True
        t.start()

    def poll_BTC(self):
        client = requests.session()
        while True:
            try:
                response = client.get(CBBTC)
                data = response.json()
                BTC = float(data['data']['rates'][self.config['currency']])
                # empty the queue if needed.
                with self.BTC_queue.mutex:
                    self.BTC_queue.queue.clear()
                self.BTC_queue.put(BTC)
            except:
                pass
            time.sleep(60)

    def get_BTC(self):
        BTC = self.BTC_queue.get()
        self.BTC_queue.put(BTC)
        log.verbose("Current BTC Price: %.2f", BTC)
        return BTC

    # Once everything is loaded we will want to poll the profits
    def main_process(self, miners):

        # Much of my testing has been with zpool and nicehash.  zpool
        # set as the primary. zpool's API times out if you access it too quickly.
        # while this shouldn't normally happen, it certainly can happen if you
        # exit and restart the miner process.  Since all pools are initialized as
        # as down, there's a chance a backup pool could come online before your
        # primary pool and then it will start mining on a backup.  This is just
        # annoying.  So, we're going to give the primary pool 30 seconds to come
        # online at first startup.  If it doesn't, then we'll assume the primary
        # pool is really down, and let the pool logic select a secondary.

        now = datetime.datetime.now()
        for pool in self.pool_status:
            if pool == 'current':
                continue
            if self.pool_status[pool]['primary'] == True:
                break
        primary_up = False
        while ( (now - self.startup_time).total_seconds() < 30):
            now = datetime.datetime.now()
            # check is the primary pool is up.
            if self.pool_status[pool]['alive'] == True:
                primary_up = True
                break
            time.sleep(1)

        if primary_up == True:
            log.info("Primary pool '%s' is alive.", self.active_pools[pool].pool_name)
        else:
            log.info("Primary pool '%s' is not respding after 30 seconds.", self.active_pools[pool].pool_name)

        t = Thread(target=self.main_loop,
                    name='gpu-poll'.format(self.__class__.__name__), args=(miners,))
        t.daemon = True
        t.start()
    # load the pools (python) from the ini file configured.
    # if there isn't a matching python file for the pool name
    # an exception will be thrown.
    # This will make adding pools easier than having to make several changes
    # throughout the code for specific pool names
    def load_pools(self):
        pool_module = __import__("pools")

        for pool in self.config['pools']:
            log.info("Instantiating %s.", pool)
            try:
                pool_class = getattr(pool_module, pool)
                pool_instance = getattr(pool_class, pool)
                self.active_pools[pool] = pool_instance(self)
                # This is mostly used for non-pool switching
                # and backup pools
                self.pool_status[pool] = {}
                if 'current' not in self.pool_status:
                    self.pool_status[pool]['primary'] = True
                    self.pool_status[pool]['secondary'] = False
                    self.pool_status[pool]['alive'] = False
                    self.pool_status['current'] = self.active_pools[pool]
                else:
                    self.pool_status[pool]['primary'] = False
                    self.pool_status[pool]['secondary'] = True
                    self.pool_status[pool]['alive'] = False
                time.sleep(1)
            except AttributeError:
                log.error("Pool '%s' is not a valid pool.", pool)
                exit(0)
            except Exception as e:
                log.error(traceback.format_exc())
                exit(0)

    def get_profits(self):
        profits = self.profits_queue.get()
        self.profits_queue.put(BTC)
        return profits

   # returns the primary pool
    def get_primary_pool(self):
        for pool in self.pool_status:
            if pool == 'current':
                continue
            if self.pool_status[pool]['primary'] == True:
                return pool

   # returns the next alive pool
    def get_alive_pool(self):
        for pool in self.pool_status:
            if pool == 'current':
                continue
            if self.pool_status[pool]['alive'] == True:
                return pool
        # if there are none, then just return the primary.
        return self.get_primary_pool()

    # use logic to switch pools if the pool is down, reverted, etc, etc
    def get_new_pool(self):

        current = self.pool_status['current']

        if self.config['usebackup'] == False:
            return current

        # if my current pool is down, go get another one
        current_pool_name = self.pool_status['current'].pool_name
        if current.pool_handler.pool_status[current_pool_name]['alive'] == False:
            # current pool is down. We only care if we're using a backup
            # pool switching will handle a down pool automatically because it
            # just won't provide data.
            new_pool = self.get_alive_pool()
            self.pool_status['current'] = self.active_pools[new_pool]
            return self.pool_status['current']


        if self.config['revertoriginal']:
            # If we're on a secondary pool, see if the primary is backup.
            if self.pool_status[current_pool_name]['secondary'] == True:
                # get the primary pool...
                primary_pool = self.get_primary_pool()
                if self.pool_status[primary_pool]['alive'] == True:
                    self.pool_status['current'] = self.active_pools[primary_pool]
                    return self.pool_status['current']

        # nothing matches, we've not changed pools.
        return current


    # This is an important function.  First, it reduces the data set
    # To just the unique values, by GPU.
    # Then, it goes through and groups the profits by GPU/algorithm/pool
    # If you're not pool switching, or split gpu mining, then this will always
    # return one row. What to mine.
    # If you are split mining, then it could potentially return as many rows
    # as you have GPUs, or more likely, a grouping of GPUs by algo.
    def what_to_mine(self, profits):
        unique=[]
        # get the unique values
        devices = (self.config['devices'] + ",total").split(",")
        for device in devices:
            for p in profits:
                if p['gpu'] == device:
                    unique.append(p)
                    break

        #Now, we have a list of the most profitable algorithms and the GPU
        # they are on. We need to sort by algo again, so we can group
        # by GPU
        unique.sort(key=itemgetter('algo'), reverse=True)

        # This does all the magic for the devices.
        mine = []
        for record, items in groupby(unique, key=itemgetter('algo','pool')):
            md=[]
            profit = 0
            for i in items:
                md.append(i['gpu'])
                profit += i['profit']
            devices = ",".join(md)
            mine.append({'algo': record[0], 'pool': record[1],
                         'devices': devices,'profit': profit})
        return mine

    def extract_profits(self, profits, current):
        # get what we're currently mining so we can compare profits
        profit = 0
        if current:
            algo_list = self.mining.get_current_mining()
        else:
            algo_list = [p['algo'] for p in profits]

        for algo in algo_list:
            for p in profits:
               if p['algo'] == algo:
                   profit = profit + p['profit']
        return profit

    # Threaded. This will grab the profits.
    # It will make decisions on what to mine
    # and it will submit algorithms to the mining queue
    def main_loop(self, miners):
        while True:

            current_pool = self.pool_status['current']
            new_pool = self.get_new_pool()
            if new_pool != current_pool:
                log.info("Changing pool from '%s' to '%s'", current_pool.pool_name,
                         new_pool.pool_name,)

            # If benchmarking is queued we don't want to do any of this junk
            # Because there aren't any profits available yet.
            if self.mining.is_benchmarking:
                time.sleep(10)
                continue
            
            profits = self.profit_by_gpu()
            if not profits:
                time.sleep(1)
                continue

            current_profit = self.extract_profits(profits, True)
            # So, now we're going to figure out what to mine
            mine_list = self.what_to_mine(profits)
            new_profit = self.extract_profits(mine_list, False)
            
            if current_profit+new_profit > 0:
                # See if the % difference between old and new profits are close
                perc_difference = int((abs(current_profit - new_profit)/
                                 ((current_profit+new_profit)/2))*100)
            perc_difference=0

            # log.debug(current_profit)
            # log.debug(new_profit)
            # log.debug(perc_difference)
            if self.config['split_gpu'] == True:
                log.info("Mining profit breakdown:")

            for mine in mine_list:
                if mine['devices'] == 'total':
                    gpu_out = ''
                    space = ''
                    devices = self.config['devices']
                else:
                    gpu_out = 'GPU ' + mine['devices'] + ": "
                    space = '  '
                    devices= mine['devices']

                log.info("%sMost profitable algo: %s'%s' " +
                         "on pool '%s' %.2f/day %s", space, gpu_out,
                         mine['algo'], mine['pool'], mine['profit'],
                         self.config['currency'])
                if perc_difference > self.config['perc_prof_diff']:
                    port = self.config['algos'][mine['algo']]['miner']['port']
                    api = self.config['algos'][mine['algo']]['miner']['api']
                    miners.submit_mining((mine['pool'], mine['algo'], port, api,
                                         devices, False))

            if (perc_difference > self.config['perc_prof_diff'] and
                current_profit >0):
                log.info("Profits of old coins: %.2f/day %s", current_profit,
                          self.config['currency'])
                log.info("Profits of new coins: %.2f/day %s", new_profit,
                          self.config['currency'])
                log.info("Profit difference is %i%%.", perc_difference)


            time.sleep(60)
            #time.sleep(60*int(self.config['min_mining_time']))

    def profit_by_gpu(self):
        ## self.__class__.__name__

        # loop through devices, and then pools.
        # At then end we'll sort by $$$
        BTC = self.get_BTC()
        # I like dicts, but they are a pain to sort.
        # We can use a list, sort it and filter it easily.
        # Then we can make decisions about pools, algs, etc
        profits = []

        devices = (self.config['devices'] + ",total").split(",")
        for device in devices:
            for pool in self.active_pools:
                if self.pool_status[pool]['alive'] == False:
                    continue
                pool_data = self.active_pools[pool].get_pool_data()
                for algo in self.active_pools[pool].my_algos.split(","):
                    unit = 'gpu'+device
                    if device == 'total':
                        unit = 'total'
                    speed = self.config['algos'][algo]['speed'][unit]
                    import random
                    algo_profit = (pool_data[algo]['btcghday']*
                                    speed/1000000000*BTC)
                    # Take fees into consideration, if we know them.
                    # assume it's
                    algo_profit -= algo_profit*(pool_data[algo]['fees']/100)
                    profits.append({'gpu': device,
                                    'pool': pool,
                                    'algo': algo,
                                    'profit': algo_profit})

        # Now sort it by profitability
        profits.sort(key=itemgetter('profit'), reverse=True)

        # We're only going to return valid, minable, entries
        # If we're not doing split GPU profits then we just want the totals
        if self.config['split_gpu'] == False:
            profits = [p for p in profits if p['gpu'] == 'total']
        else:
            profits = [p for p in profits if p['gpu'] != 'total']

        # if we're not pool switching, we want to filter out the other
        # pool's data
        if not self.config['poolswitching']:
            profits = [p for p in profits if p['pool'] == self.pool_status['current'].pool_name]

        with self.profits_queue.mutex:
            self.profits_queue.queue.clear()
        self.profits_queue.put(profits)

        return profits
