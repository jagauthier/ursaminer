#!/usr/bin/python

import ConfigParser
import time
import random
import json
import os

config_data = ConfigParser.ConfigParser()
config_data.read('benchmark2.ini')

algos="phi,polytimos,hsr,keccak,xevan,skunk,tribus,c11,x11evo,lbry" + \
       ",skein,equihash,groestl,timetravel,sib,bitcore,x17,blakecoin,nist5," + \
       "myr-gr,lyra2v2,neoscrypt,blake2s,cryptonight," + \
       "lyra2re,pascal"

blake2s='{\
    "Live":  3009952750.0086,\
    "Minute":  3009952750.0086,\
    "Minute_Fluctuation":  0,\
    "Minute_5":  3009952750.0086,\
    "Minute_5_Fluctuation":  0,\
    "Minute_10":  3009952750.0086,\
    "Minute_10_Fluctuation":  0,\
    "Hour":  3009952750.0086,\
    "Hour_Fluctuation":  0,\
    "Day":  3027706652.99504,\
    "Day_Fluctuation":  0.00628099607217342,\
    "Week":  3009952750.0086,\
    "Week_Fluctuation":  0.056893769104427894,\
    "Updated":  "\/Date(1516671990708)\/" \
}'


def convert_algo(algo):
    if algo == "myriadgroestl":
        return "myr-gr"
    if algo == "lyra2re2":
        return "lyra2v2"
    return algo

source = './stats'
algos={}
for root, dirs, filenames in os.walk(source):
    for f in filenames:

        algo_name = f.split("_")[1].lower()
        algo_name = convert_algo(algo_name)
        fullpath = os.path.join(source, f)
        data = json.load(open(fullpath))

        algos[algo_name] = data['Live']

print algos


bench = ConfigParser.RawConfigParser()
for algo_name in algos:
    bench.add_section(algo_name)

    bench.set(algo_name, 'total', algos[algo_name])
    bench.set(algo_name, 'gpu1', algos[algo_name]*.66)
    bench.set(algo_name, 'gpu0', algos[algo_name]*.33)
with open('bench.ini', 'wb') as configfile:
    bench.write(configfile)
