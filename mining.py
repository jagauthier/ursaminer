#!/usr/bin/python
import sys
import time
import logging
import socket
import Queue
import shlex
import subprocess
import datetime
import os
import socket
from time import strftime
from threading import Thread
from multiprocessing import Process

from pools.pool_handler import pool_handler
from utils import utils

ID = 'UrsaMiner-1.0'
log = logging.getLogger(__name__)

# Loop through unbenchmarked pools and benchmark the
# algorithms that are assigned and need to be benchmarked.

class mining():
    pools_global = None
    config = None
    active_miners = {}
    submit_queue = Queue.Queue()
    benchmarking = False
    mon_control = {}

    def __init__(self, config, pools_global):
        self.pools_global = pools_global
        self.pools_global.mining = self
        self.config = config
        log.debug("Starting thread: %s", self.__class__.__name__)
        t = Thread(target=self.run,
                   name='{}'.format(self.__class__.__name__), args=())
        t.daemon = True
        t.start()
    #    self.benchmark()

    def is_benchmarking(self, algo=None):
        for l_algo in self.active_miners:
            log.debug("Checking %s against %s", algo, l_algo)
            # Check against a specific algo
            if algo == l_algo:
                return True
            # check if we're benchmarking -  at all
            if algo==None and self.active_miners[l_algo]['benchmarking']:
                return True                
        return False

    def get_current_mining(self):
        mining = []
        for l_algo in self.active_miners:
            if self.active_miners[l_algo]['processid'].poll() is None:
                mining.append(l_algo)
        return mining

    def check_active_mining(self,miner_info):
        mine_data = miner_info[0]
        pool = mine_data[0]
        algo = mine_data[1]
        port = mine_data[2]
        ap = mine_data[3]
        devices = mine_data[4]
        benchmark = mine_data[5]
        bin_id = miner_info[1][0]

        stay = True
        # clean up any defunct miner records from the active_miners dict..
        while stay:
            for l_algo in self.active_miners:
                if self.active_miners[l_algo]['processid'].poll() is not None:
                    l_pool = self.active_miners[l_algo]['pool']
                    log.debug("Removed inactive miner for '%s' on '%s' using '%s'",
                              l_algo, l_pool,
                              self.active_miners[l_algo]['bin_id'])

                    del self.active_miners[l_algo]
                    stay = True
                    break
            stay = False

        # See if we're benchmarking anything. If we have benchmarks,
        # we're not stopping, or inserting anything.
        if self.is_benchmarking():
            return False


        # if we're doing specific GPU mining we need to look for the devices
        # otherwise, we just need to look for the matching device (ie 0,1)
        if self.config['split_gpu'] == True:
            for l_algo in self.active_miners:
                if l_algo == algo and self.active_miners[l_algo]['devices'] == devices:
                    # an exact match. Which means the algo and devices assigned did not change
                    log.debug("Already mining '%s' on '%s' with GPU %s", algo, pool, devices)
                    return False
            # No match
            # That means we want to kill every miner using any of the devices
            # we're asking for.

            for l_algo in self.active_miners:
                for device in devices.split(","):
                    if device in self.active_miners[l_algo]['devices']:
                        l_pool = self.active_miners[l_algo]['pool']
                        log.debug("GPU %s already mining '%s' on '%s'. Going to kill it.",
                                  device, l_algo, l_pool)
                        # If it's running, kill it. It might not be...
                        # If the miner poo'd out. So do a check
                        if self.active_miners[l_algo]['processid'].poll() is None:
                            self.active_miners[l_algo]['processid'].kill()

            # whether we found something or not.. we're going to start mining.
            return True


        else:
            # not split mining, which means there will only be one active miner
            # at a time. If the algo is not even in the dict, then we're not
            # currently mining it. We should stop what we're doing and start it.

            if algo not in self.active_miners:
                for l_algo in self.active_miners:
                    # kill anything
                    # next time this process runs, it should appear defunct
                    # and be removed. There really only should be one of these
                    # but weird things could happen
                    l_pool = self.active_miners[l_algo]['pool']
                    l_devices= self.active_miners[l_algo]['devices']
                    log.debug("GPU %s already mining '%s' on '%s'. Going to kill it.",
                              l_devices, l_algo, l_pool)
                    # If it's running, kill it. It might not be...
                    # If the miner poo'd out. So do a check
                    if self.active_miners[l_algo]['processid'].poll() is None:
                        self.active_miners[l_algo]['processid'].kill()
                return True
            else:
                log.debug("Already mining '%s' on '%s' with GPU %s", algo, pool, devices)
                return False


    def run(self):
        # This will do nothing if there is nothing to do
        # Okay, so we got a mining requests. Let's see what it is.
        while True:
            miner_info = self.submit_queue.get()
            mine_data = miner_info[0]
            pool = mine_data[0]
            algo = mine_data[1]
            port = mine_data[2]
            api = mine_data[3]
            devices = mine_data[4]
            benchmark = mine_data[5]

            bin_id = miner_info[1][0]
            cmd = miner_info[1][1]

            # before we do anything, we need to device is we're already mining
            if not self.check_active_mining(miner_info):
                continue

            # Going to make a dict of Queues
            # So, when the miner stop, we can tell the thread to stop too
            self.mon_control[algo] = Queue.Queue()

            # The goal:  Monitor the output of the miner, write it a file,
            # while being able to stop the thread it's running on.
            # The problem: Python threads can't be killed externally.
            # Additionally, popen()'s readline is blocking.
            # The solution: If we start the process here, and then pass
            # the processid to the thread, it can block happily on readline()
            # all day.  When the task is told to die (it will be told), then
            # readline will return, and popen.poll will not be None, and it
            # exits. This took me way to long to solve. But now It's a very
            # elegant solution to work around limitations.

            try:
                # DEV HACK
                cmd = "python " + cmd
                processid = subprocess.Popen(shlex.split(cmd), shell=False,
                                             stdout=subprocess.PIPE,
                                             stderr=subprocess.STDOUT)
            except OSError as e:
                log.error("There was an error starting the '%s' miner.", bin_id)
                log.error("%s", e.strerror)
                log.error('%s', cmd)
                # we'll not completely fail.
                continue

            # we're going to start the thread to monitor the output and write
            # it to a file.
            log_thread = Thread(target=self.log_miner,
                                 name='miner-{}'.format(algo), args=(processid, miner_info))
            log_thread.daemon = True
            log_thread.start()

            # Now, we're going to start the thread that talks to the miner's API.
            # I want this thread to exit when the miner exits.
            mon_thread = Thread(target=self.monitor_miner,
                                            name='mon-{}'.format(algo),
                                            args=(processid,miner_info))
            mon_thread.daemon = True
            mon_thread.start()

            # This is a dictionary of the miners, for this class.
            # We'll use it when we're not benchmarking
            # but benchmarking will be a linear process, so we can
            # just use the variables we're defining directly.
            earliest_end = (datetime.datetime.now() +
                           datetime.timedelta(
                           minutes=self.config['min_mining_time']))
            self.active_miners[algo] = {'pool': pool,
                                        'algo': algo,
                                        'port': port,
                                        'api': api,
                                        'benchmarking': benchmark,
                                        'start': datetime.datetime.now(),
                                        'earliest_end': earliest_end,
                                        'miner_info': miner_info,
                                        'log_thread': log_thread,
                                        'processid': processid,
                                        'queue': self.mon_control[algo],
                                        'devices': devices,
                                        'bin_id': bin_id}

            # if we're benchmarking, they are all going to queue up.
            # because we only want to run one bench at a time.
            # if we're mining, it's possible that we will want to mine
            # more than one crypto at a time on different cards
            if not benchmark:
                continue

            start = datetime.datetime.now()
            while processid.poll() == None:
                now = datetime.datetime.now()
                if (now - start).total_seconds() > self.config['benchmark_time']:
                    processid.kill()
                    self.mon_control[algo].put("stop")
                    del self.active_miners[algo]
                time.sleep(1)

    def log_miner(self, processid, miner_info):

        mine_data = miner_info[0]
        pool = mine_data[0]
        algo = mine_data[1]
        devices = mine_data[4]
        benchmark = mine_data[5]
        cmd = miner_info[1][1]
        log.info("Start: '%s' on '%s'. Benchmark = %r.", algo, pool, benchmark)
        log.debug("Running '%s'", cmd)
        if not os.path.isdir(self.config['miner_logs']):
            os.mkdir(self.config['miner_logs'])

        mp_path = os.path.join(self.config['miner_logs'], pool +
                         "_" + algo + ".log")
        # log all mining to one file so it can be monitored
        m_path = os.path.join(self.config['miner_logs'], "miner.log")

        # keep mining logs for all time (until user deletes them anyway)
        mp_out = open(mp_path, "a")
        m_out = open(m_path, "a")
        while True:
            if processid.poll() == None:
                output = processid.stdout.readline()
                if len(output) > 1:
                  mp_out.write(output)
                  m_out.write("[" + pool + "][" + algo + "]: " + output)
                  # flush so we can tail the logs realtime.
                  mp_out.flush()
                  m_out.flush()
            else:
                # In case this thread exits itself.. we want to stop
                # the monitoring thread too.
                self.mon_control[algo].put("stop")
                break
        log.info("Stop: '%s' on '%s'. Benchmark = %r.", algo, pool, benchmark)
        mp_out.close()
        m_out.close()
        return

    def miner_data_request(self, algo, port, type):
        s = None
        # seems like a good default
        data = "error"
        # not really used, except for testing/debugging, defaults to localhost
        hostname = self.config['algos'][algo]['miner']['hostname']
        # get the stuff
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((hostname, port))
            s.sendall(type)
            data = s.recv(4096)
        except Exception:
            data = "error"

        return data

    def store_hashrate(self, data, gpu_hash):
        gpus={}
        # Split the lines, which end with a |
        gpu_info=data.split('|')
        for gpu in gpu_info:
            # weird characters at the end of the last |
            if len(gpu)>1:
                # split the semi-comma seperated values, and then split
                # the = into a referencable dict object
                d = dict(x.split('=') for x in gpu.split(';'))
                gpuid=d['GPU']
                # update it if it exists, else create a new one
                # multiply by 1000, so we have this in hashes not khs
                if gpuid in gpu_hash:
                    gpu_hash[gpuid].update({'KHS': float(d['KHS'])*1000})
                else:
                    gpu_hash[gpuid] = {'GPU': d['GPU'], 'KHS': float(d['KHS'])*1000}

    def monitor_miner(self, processid, miner_info):
        mine_data = miner_info[0]
        algo = mine_data[1]
        port = mine_data[2]
        api = mine_data[3]
        benchmark = mine_data[5]
        threads = None
        summary = None        
        stop = None
        gpu_hash = {}
        log.debug("Starting monitoring '%s' on '%i' using api: '%s'.", algo,
                   port, api)

        start = datetime.datetime.now()
        while True:
            # try:
            #     stop = self.mon_control[algo].get(False)
            # except Queue.Empty:
            #     if stop == "stop":
            #         # Button up all the stats, and send them to the webhook
            #         break
            if processid.poll() != None:
                break
            
            now = datetime.datetime.now()
            tdiff = (now-start).total_seconds()
            
            # ccminer API section
            if api == "ccminer":
                threads = self.miner_data_request(algo, port, "threads")
                summary = self.miner_data_request(algo, port, "summary")                
                if threads != "error" and summary != "error":
                    # Good api results, then we'll store the benchmark data
                    if tdiff > self.config['benchmark_time']*.5:
                        self.store_hashrate(threads, gpu_hash)

                    #log.debug(data)
                else:
                    log.debug("Nothing from API.  Miner could be stopped, " +
                              "or not even running. Don't care. We'll stop " +
                              "when we're told to stop via the queue.")
                              
            perc_comp=int(tdiff*100/self.config['benchmark_time'])
            if benchmark:
                if perc_comp > 0 and int(tdiff % 30) == 0:
                    log.info("Benchmarking '%s' %i%% complete.", algo, perc_comp)
                
            # We're only going to get data once per second
            # it could cause a slight delay from when the miner is told to quit
            # and we get the signal, but it shouldn't be a big deal.
            time.sleep(1)

        # If we've run long enough, then we should record the speed
        if self.config['benchmark_time']*.5 and gpu_hash:
            log.debug("Saving benchmark for '%s', for GPUs '%s'",algo, ",".join(gpu_hash))
            total = 0
            self.config['algos'][algo]['speed'] = {}
            for gpu in gpu_hash:
                self.config['algos'][algo]['speed']['gpu'+gpu] = (
                                                          gpu_hash[gpu]['KHS'])
                total += gpu_hash[gpu]['KHS']
                log.info("GPU %s: %s %s",gpu, algo,
                         utils.convert_khs(algo, gpu_hash[gpu]['KHS']))
            self.config['algos'][algo]['speed']['total'] = total
            self.config['algos'][algo]['needs_benchmark'] = False
            log.info("Total: %s %s", algo, utils.convert_khs(algo,total))

            utils.write_benchmark(self.config, algo)
        log.debug("Stopped monitoring '%s'.", algo)

    def submit_mining(self, mine_data):
        # I don't need to unwrap these here, but it helps me remember what's
        # in the data
        pool = mine_data[0]
        algo = mine_data[1]
        port = mine_data[2]
        api = mine_data[3]
        devices = mine_data[4]
        benchmark = mine_data[5]

        # Before we can submit this, we need to see if the port is in use.
        # We could be split mining, or who knows. If the port is in use, we'll
        # keep trying until we find a new port.
        hostname = self.config['algos'][algo]['miner']['hostname']
        open_port = False

        while open_port == False:
            open_port = utils.check_socket(hostname , port)
            if (open_port):
                port = port + 1
                # Put it all back
                mine_data = (pool, algo, port, api, devices, benchmark)
            else:
                break

        # !!TODO delete this! It's for testing only!!
        port = 4068
        mine_data = (pool, algo, port, api, devices, benchmark)
        # !!Delete to here!!


        cmd_info = self.build_command(mine_data)
        miner_info = (mine_data, cmd_info)
        self.submit_queue.put(miner_info)

    def build_command(self, mine_data):
        # Get the pool info
        pool = mine_data[0]
        algo = mine_data[1]
        port = mine_data[2]
        devices = mine_data[4]
        mine_port = "-b 127.0.0.1:" + str(port) + " "
        mine_algo = "-a " + algo + " "
        devices = "-d " + devices + " "
        username = "-u " + self.config[pool]['username'] + " "
        password = "-p ID=" + ID + "," + self.config[pool]['password'] + " "
        pool_data = self.pools_global.active_pools[pool].get_pool_data()
        stratum = "-o " + pool_data[algo]['stratum_host'] + " "
        options = self.config['algos'][algo]['miner']['options'] + " "
        # Potential here to select a miner/algo combo
        # if we have multiple miners configured for a single algo
        bin_loc = os.path.join(self.config['miner_path'],
                               self.config['algos'][algo]['miner']['bin']) + " "
        bin_loc = self.config['miner_path'] + "/" + self.config['algos'][algo]['miner']['bin'] + " "
        cmd = bin_loc + mine_port + mine_algo + devices + options + stratum + username + password
        #log.debug("'%s': '%s'", algo, cmd)
        return ( (self.config['algos'][algo]['miner']['bin'], cmd) )
