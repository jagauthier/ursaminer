#!/usr/bin/python
from __future__ import print_function  # accomodate Python 2
#from colors import *
import requests
import datetime
import sys
import time
import getopt
import logging
import os
import pprint
from time import strftime

from pools.pool_handler import pool_handler
from utils import utils
from mining import mining

pools_global = None

class LogFilter(logging.Filter):

    def __init__(self, level):
        self.level = level

    def filter(self, record):
        return record.levelno < self.level

# So the logging is configured at load time.
formatter = logging.Formatter(
    '%(asctime)s [%(threadName)18s][%(module)14s][%(levelname)8s] %(message)s')

# Redirect messages lower than WARNING to stdout
stdout_hdlr = logging.StreamHandler(sys.stdout)
stdout_hdlr.setFormatter(formatter)
log_filter = LogFilter(logging.WARNING)
stdout_hdlr.addFilter(log_filter)
stdout_hdlr.setLevel(5)
# Redirect messages equal or higher than WARNING to stderr
stderr_hdlr = logging.StreamHandler(sys.stderr)
stderr_hdlr.setFormatter(formatter)
stderr_hdlr.setLevel(logging.WARNING)

log = logging.getLogger()
log.addHandler(stdout_hdlr)
log.addHandler(stderr_hdlr)

log.setLevel(logging.INFO)


def set_log_and_verbosity(log, config):

    # set a custom logging level for really insane stuff
    logging.VERBOSE = 5
    logging.addLevelName(logging.VERBOSE, "VERBOSE")
    logging.Logger.verbose = lambda inst, msg, *args, **kwargs: inst.log(logging.VERBOSE, msg, *args, **kwargs)
    logging.verbose = lambda msg, *args, **kwargs: logging.log(logging.VERBOSE, msg, *args, **kwargs)

    # Always write to log file.
    if config['logfile'] != 'stdout':
        if not os.path.exists(config['logdir']):
            os.mkdir(config['logdir'])

        date = strftime('%Y%m%d_%H%M')
        filelog = logging.FileHandler(config['logdir']+config['logfile'])
        filelog.setFormatter(logging.Formatter(
            '%(asctime)s [%(threadName)18s][%(module)14s][%(levelname)8s] ' +
            '%(message)s'))
        log.addHandler(filelog)

    # This sneaky one calls log.warning() on every retry.
    urllib3_logger = logging.getLogger(requests.packages.urllib3.__package__)
    urllib3_logger.setLevel(logging.ERROR)

    if config['verbose'] or config['veryverbose']:
        log.setLevel(logging.DEBUG)
        logging.getLogger('requests').setLevel(logging.DEBUG)
        urllib3_logger.setLevel(logging.INFO)

    if config['veryverbose']:
        log.setLevel(logging.VERBOSE)


def report_output(startup, current):

    sys.stderr.write("\x1b[2J\x1b[H")

    print (color('Time', fg='white', style='bold'))
    print ("  Start  :", startup['time'].strftime("%m/%d/%Y %H:%M:%S"))
    print ("  Now    :", current['time'].strftime("%m/%d/%Y %H:%M:%S"))
    days = divmod((current['time'] - startup['time']).total_seconds(), 86400)
    hours = divmod(days[1], 3600)
    mins = divmod(hours[1], 60)
    secs = mins[1]

    print ("  Elapsed: ", end='')
    if (days[0]):
        if days[0] == 1:
            print ("1 day", end='')
        else:
            print ("%i days", days[0], end='')

    print (" %.2i:%.2i:%.2i" % (hours[0], mins[0], secs))
    print ("\nBTC: " + color(BTC, fg='white', style='bold') + "\n")
    start_val = round(BTC * startup['total'], 2)
    curr_val = round(BTC * current['total'], 2)
    print ("Starting  : " + str("%.8f" %
           startup['total']) + " ($" + str("%.2f" % start_val) + ")")
    print ("Current   : " + str("%.8f" %
           current['total']) + " ($" + str("%.2f" % curr_val) + ")")
    difference_btc = current['total'] - startup['total']
    difference_usd = curr_val - start_val
    print ("Difference: " + str("%.8f" % difference_btc) +
           " ($" + str("%.5f" % difference_usd) + ")\n")

    elapsed = (current['time'] - startup['time']).total_seconds()
    # This is just for prediction
    print (color('Statistics:', style='bold'))
    if (elapsed < 60):
        per_minute_btc = (60 * difference_btc) / elapsed
    else:
        per_minute_btc = difference_btc / (elapsed / 60)
    per_minute_usd = round(per_minute_btc * BTC, 4)

    if (elapsed < 3600):
        per_hour_btc = (3600 * difference_btc) / elapsed
    else:
        per_hour_btc = difference_btc / (elapsed / 3600)
    per_hour_usd = round(per_hour_btc * BTC, 4)

    if (elapsed < 86400):
        per_day_btc = (86400 * difference_btc) / elapsed
    else:
        per_day_btc = difference_btc / (elapsed / 86400)
    per_day_usd = round(per_day_btc * BTC, 4)

    if (elapsed < 86400 * 7):
        per_week_btc = (86400 * 7 * difference_btc) / elapsed
    else:
        per_week_btc = difference_btc / (elapsed / 86400 * 7)
    per_week_usd = round(per_week_btc * BTC, 4)

    if (elapsed < 86400 * 31):
        per_month_btc = (86400 * 31 * difference_btc) / elapsed
    else:
        per_month_btc = difference_btc / (elapsed / (86400 * 31))
    per_month_usd = round(per_month_btc * BTC, 4)

    print ("  Per minute: " + str("%.8f" % round(per_minute_btc, 7)) +
           " ($" + str("%.5f" % per_minute_usd) + ")")
    print ("  Per hour  : " + str("%.8f" % round(per_hour_btc, 7)) +
           " ($" + str("%.5f" % per_hour_usd) + ")")
    print ("  Per day   : " + str("%.8f" % round(per_day_btc, 7)) +
           " ($" + str("%.5f" % per_day_usd) + ")")
    print ("  Per week  : " + str("%.8f" % round(per_week_btc, 7)) +
           " ($" + str("%.5f" % per_week_usd) + ")")
    print ("  Per month : " + str("%.8f" % round(per_month_btc, 7)) +
           " ($" + str("%.5f" % per_month_usd) + ")")

    print (color('\nCurrently mining:', style='bold'))

    for miner in current['miners']:
        if int(miner['rate'] > 1000000000):
            rate = round(miner['rate'] / 1000000000, 2)
            rate_str = "GH/s"
        else:
            if int(miner['rate'] > 1000000):
                rate = round(miner['rate'] / 1000000, 2)
                rate_str = "MH/s"
            else:
                rate = round(miner['rate'] / 1000, 2)
                rate_str = "Kh/s"
        print("  " + miner['algo'].ljust(8) +
              ": " + str(rate) + " " + rate_str)


if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], "c:")
    except:
        print ("poolmon.py -c config")
        exit(1)

    starting_currency = None
    starting_time = None

    config_file = "config.ini"
    for opt, arg in opts:
        if opt == '-c':
            config_file = arg

    config = utils.load_config(config_file)
    set_log_and_verbosity(log, config)

    log.info("Starting.")

    pools_global = pool_handler(config)

    #pprint.pprint(config)

    miners = mining(config, pools_global)

    pools_global.load_pools()
    
    pools_global.main_process(miners)

    
    
    while True:
        time.sleep(5)
    exit(0)

    pool.get_pool_data(client, config['location'])

    # get the starting values
    pool.get_my_data(client, address)

    # copy the dictionary/
    startup = pool.my_info.copy()

    if starting_currency is not None:
        startup['total'] = starting_currency
    if starting_time is not None:
        startup['time'] = starting_time
    # create a starting set of data
    current = {}
    current['total'] = startup['total']
    current['time'] = datetime.datetime.now()


    # Loop here
    while (1):
        # If this fails, then BTC will remain unchanged.
        BTC = utils.get_BTC(BTC, client)
        pool.get_my_data(client, address)

        report_output(startup, pool.my_info)
        time.sleep(15)
