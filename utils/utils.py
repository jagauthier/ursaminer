#!/usr/bin/python

# This will contain utility functions as well as config handling

import ConfigParser
import time
import ConfigParser
import sys
import logging
import platform
import socket
from contextlib import closing

M = 1000000
G = 1000000000

log = logging.getLogger(__name__)

def memoize(function):
    memo = {}

    def wrapper(*config):
        if config in memo:
            return memo[config]
        else:
            rv = function(*config)
            memo[config] = rv
            return rv
    return wrapper

def check_socket(host, port):
    try:
        log.verbose("Checking availability of port '%s:%s'", host, port)
        with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
            if sock.connect_ex((host, port)) == 0:
                # Socket is responding
                return True
            else:
                return False
    except:
        # Any problems.. no good
        return False

def load_config_miner(config_data, config, algo, miner_string, miner_dict):
    # Check if the miner binary has been configured
    if config_data.has_section(miner_string):
        miner_dict['api'] = "ccminer"

        if config_data.has_option(miner_string, 'bin'):
            miner_dict['bin']=config_data.get(miner_string, 'bin')
        else:
            log.error("'%s' [%s] missing option 'bin'.",
                      config['miners'], miner_string)
            exit(0)
        # Load the URL from the platform name
        # Not required in case you want to put your own binaries
        # in place, without downloading them
        if config_data.has_option(miner_string, platform.system()):
            miner_dict['url']=config_data.get(miner_string,
                                              platform.system())
        # check for non-ccminer API
        if config_data.has_option(miner_string, 'api'):
            miner_dict['api']=config_data.get(miner_string, 'api')

        # check for specific port requested
        if config_data.has_option(miner_string, 'port'):
            miner_dict['port']=config_data.get(miner_string, 'port')
        else:
            miner_dict['port']=config['port']

        miner_dict['hostname']=config['hostname']

    else:
        log.error("'%s' missing section [%s]", config['miners'],
                  miner_string)
        exit(0)

def load_config_algorithms(pools, config):
    # read in the algorithms from miner file
    config_data = ConfigParser.ConfigParser()
    config_data.read(config['miners'])

    # all these defaults can be set in the ini file
    config['miner_logs'] = "miner_logs"
    config['miner_path'] = "bin"
    config['port'] = 4068
    config['hostname']='localhost'

    if config_data.has_option('general', 'miner_logs'):
        config['miner_logs'] = config_data.get('general',
                                               'miner_logs')
    if config_data.has_option('general', 'miner_path'):
        config['miner_path'] = config_data.get('general',
                                               'miner_path')

    if config_data.has_option('general', 'devices'):
        config['devices'] = config_data.get('general', 'devices')

    if config_data.has_option('general', 'port'):
        config['port'] = config_data.get('general', 'port')

    if config_data.has_option('general', 'hostname'):
        config['hostname'] = config_data.get('general', 'hostname')


    if config_data.has_option('general', 'cc_options'):
        config['cc_options'] = config_data.get('general', 'cc_options')

    try:
        with open(config['algofile']) as f:
            for algo in f:
                config['algos'][algo.strip()] = {}
                # set it to not in use here.
                config['algos'][algo.strip()]['inuse'] = False
    except:
        log.error("Could not read '%s'", config['algofile'])

    for pool in pools:
        for algo in config[pool]['algos'].split(","):
            if config_data.has_section(algo):
                # create empty dicts
                if algo not in config['algos']:
                    log.error("'%s' is missing algo named '%s'." +
                              " Please add it and make sure it's a valid " +
                              "algo on your selected pools.", config['algofile'],
                              algo)
                    exit(0)
                config['algos'][algo]['inuse'] = True
                config['algos'][algo]['miner'] = {}
                miner_dict = config['algos'][algo]['miner']
                # check for the miner option in the algorithm
                if config_data.has_option(algo, 'miner'):
                    if config_data.has_option(algo, 'miner'):
                        miner_string = config_data.get(algo, 'miner')
                    miner_dict['miner'] = miner_string
                    load_config_miner(config_data, config, algo,
                                      miner_string,  miner_dict)
                    if 'cc_options' in config:
                        miner_dict['options'] = config['cc_options'] + " "
                else:
                    log.error("Section '%s' in '%s' missing " +
                              "'miner' setting.", algo,
                              config['miners'])
                    exit(0)
                if config_data.has_option(algo, 'options'):
                    options = config_data.get(algo, 'options')
                    miner_dict['options'] = miner_dict['options'] + options
            else:
                log.error("Missing section '%s' in '%s'", algo,
                          config['miners'])
                exit(0)


def load_config(config_path):
    # TODO Load defaults from HTTPS

    config = {}
    # set up some defaults
    # Don't change these.. use the config
    config['verbose'] = False
    config['veryverbose'] = False
    config['logdir'] = 'logs/'
    config['logfile'] = 'log.txt'
    config['poolswitching'] = False
    config['usebackup'] = False
    config['revertoriginal'] = False
    config['miners'] = 'miners.ini'
    config['algofile'] = 'algos.txt'
    config['currency'] = 'USD'
    config['min_mining_time'] = 5
    config['split_gpu'] = False
    config['perc_prof_diff'] = 10

    # not really something we want to configure, but I want it as a potential
    # variable
    config['benchmark'] = 'benchmark.ini'
    config['benchmark_time'] = 600

    config['algos'] = {}

    config_data = ConfigParser.ConfigParser()
    config_data.read(config_path)

    # load the general section first
    if config_data.has_option('general', 'currency'):
        config['currency'] = config_data.get('general', 'currency', 0)

    if config_data.has_option('general', 'location'):
        config['password'] = config_data.get('general', 'location', 0)

    if config_data.has_option('general', 'verbose'):
        config['verbose'] = config_data.getboolean('general', 'verbose')

    if config_data.has_option('general', 'veryverbose'):
        config['veryverbose'] = config_data.getboolean('general', 'veryverbose')

    if config_data.has_option('general', 'logdir'):
        config['logdir'] = config_data.get('general', 'logdir', 0)

    if config_data.has_option('general', 'perc_prof_diff'):
        config['perc_prof_diff'] = config_data.get('general', 'perc_prof_diff', 0)

    if config_data.has_option('general', ''):
        config['logfile'] = config_data.get('general', 'logfile', 0)

    if config_data.has_option('general', 'min_mining_time'):
        config['min_mining_time'] = config_data.get('general', 'min_mining_time', 0)

    if config_data.has_option('general', 'split_gpu'):
        config['split_gpu'] = config_data.getboolean('general', 'split_gpu')

    if config_data.has_option('general', 'usebackup'):
        config['usebackup'] = config_data.getboolean('general', 'usebackup')

    if config_data.has_option('general', 'revertoriginal'):
        config['revertoriginal'] = config_data.getboolean('general',
                                                          'revertoriginal')

    if config_data.has_option('general', 'poolswitching'):
        config['poolswitching'] = config_data.getboolean('general',
                                                         'poolswitching')

    if config_data.has_option('general', 'benchmark_time'):
        config['benchmark_time'] = config_data.getfloat('general',
                                                   'benchmark_time')
                                                         
    if config_data.has_option('general', 'pool'):
        config['pool'] = config_data.get('general', 'pool', 0)
    else:
        log.error("[general] section requires 'pool' in %s", config_path)
        exit(0)

    if len(config['pool'].split(",")) > 1 and (config['usebackup'] == False and
        config['poolswitching'] == False):
        log.error("You've configured multiple pools, but are not using a backup " +
                  "or pool switching.  This will only create confusion.")
        exit(0)

    if config['usebackup'] == True and config['poolswitching'] == True:
        log.warn("usebackup and poolswitching are mutually exclusive options." +
                 " Disabling usebackup.")
        config['usebackup'] = False

    if len(config['pool'].split(",")) == 1 and (config['usebackup'] == True or
        config['poolswitching'] == True):
        log.error("You've configured only a single pool, but are trying to a backup " +
                  "or pool switching.  This will only create confusion.")
        exit(0)

    log.info("Running for the following pools: %s", config['pool'])
    if config['poolswitching'] == True:
        log.info("Will switch to most profitable pool.")

    pools = config['pool'].split(",")
    # Put this into an array, it will be useful.
    config['pools'] = pools
    if config['usebackup'] == True:
        log.info("Will use %s as backup.", ",".join(pools[1:]))
        if config['revertoriginal'] == True:
            log.info("Will revert to original pool.")
        else:
            log.info("Will not revert to original pool.")
    else:
        log.info("Backup pool is not configured.")

    if config_data.has_option('general', 'algofile'):
        config['algofile'] = config_data.get('general', 'algofile', 0)


    for pool in pools:
        # load the pool information
        if config_data.has_section(pool):
            config[pool] = {}

            if config_data.has_option(pool, 'location'):
                config[pool]['location'] = config_data.get(pool, 'location', 0)
            else:
                config[pool]['location'] = 'usa'

            if config_data.has_option(pool, 'username'):
                config[pool]['username'] = config_data.get(pool, 'username', 0)
            else:
                log.error("Section [%s] does not have a username set.", pool)
                exit(0)
            if config_data.has_option(pool, 'password'):
                config[pool]['password'] = config_data.get(pool, 'password', 0)
            else:
                log.error("Section [%s] does not have a password set.", pool)
                exit(0)
            if config_data.has_option(pool, 'algos'):
                config[pool]['algos'] = config_data.get(pool, 'algos', 0)
            else:
                log.error("Section [%s] does not have algorithms set.", pool)
                exit(0)
            if config_data.has_option(pool, 'wallet'):
                config[pool]['wallet'] = config_data.get(pool, 'wallet', 0)
        else:
            log.error("%s is missing section [%s]", config_path, pool)
            exit(0)
    if config_data.has_option(config['pool'], 'password'):
        config['password'] = config_data.get(config['pool'], 'password', 0)

    load_config_algorithms(pools, config)

    return config

def get_hashing_speed(algo):
    if algo == "x17":
        return 8.41 * M
    if algo == "keccak":
        return 765.07 * M
    if algo == "neoscrypt":
        return 1.15 * M
    if algo == "lyra2rev2":
        return 19847212.85268963
    if algo == "decred":
        return 1559140438.049052
    if algo == "cryptonight":
        return 149.68814968814968
    if algo == "lbry":
        return 271.29 * M
    if algo == "equihash":
        return 486.92
    if algo == "pascal":
        return 515508191.34396356
    if algo == "sia":
        return 757573840.5084542
    if algo == "blake2s":
        return 2244087013.969037
    if algo == "bitcore":
        return 15.26 * M
    if algo == "xevan":
        return 3.26 * M
    if algo == "poly":
        return 21.11 * M
    if algo == "x11evo":
        return 12.15 * M
    if algo == "veltor":
        return 35.08 * M
    if algo == "tribus":
        return 54.72 * M
    if algo == "skein":
        return 543.72 * M
    if algo == "sib":
        return 13.72 * M
    if algo == "nist5":
        return 26.94 * M
    if algo == "timetravel":
        return 18.72 * M
    if algo == "yescrypt":
        return 1.94 * 1000
    if algo == "myr-gr":
        return 73 * M
    if algo == "lyra2v2":
        return 40.77 * M
    if algo == "hsr":
        return 11.39 * M
    if algo == 'groestle':
        return 32.39 * M
    if algo == 'c11':
        return 17.39 * M
    if algo == 'skunk':
        return 27.39 * M
    if algo == 'blakecoin':
        return 5000 * M
    if algo == 'blake2s':
        return 2991 * M
    return 0

def write_benchmark(config, algo):
    bench = ConfigParser.RawConfigParser()
    # read the current config in completely
    bench.read(config['benchmark'])
    if not bench.has_section(algo):
        bench.add_section(algo)
            
    for key in config['algos'][algo]['speed']:
        bench.set(algo, key, config['algos'][algo]['speed'][key])

    with open(config['benchmark'], 'wb') as configfile:
        bench.write(configfile)

def convert_khs(algo, khs):
    log.info("%i", khs)
    if khs < 1000:
        return "{:.2f}".format(khs) + " H/s"
    if khs < 100000:
        return "{:.2f}".format(khs/1000) + " MH/s"    
    if khs < 1000000000:
        return "{:.2f}".format(khs/1000/1000) + " MH/s"
    if khs < 1000000000000:
        return "{:.2f}".format(khs/1000/1000/1000) + " GH/s"
    if khs < 1000000000000000:
        return "{:.2f}".format(khs/1000/1000/1000/1000) + " PH/s"
